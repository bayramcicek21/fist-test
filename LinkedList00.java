package udemy_ds_00_node;

/**
 * created by cicek on 07.07.2019 19:18
 */

public class LinkedList00 {
    private Node00 header;
    private Node00 lastNode;
    private int size;

    public LinkedList00() {
        header = new Node00(null);
        lastNode = header;
    }

    public void prepend(Integer data) {
        Node00 n = new Node00(data);

        if (size == 0) {
            header.next = n;
            lastNode = n;
            size++;
        } else {
            Node00 temp = header.next;
            header.next = n;
            n.next = temp;
            size++;
        }
    }

    public void append(Integer data) {
        Node00 n = new Node00(data);

        if (size == 0) {
            header.next = n;
            lastNode = n;
            size++;
        } else {
            lastNode.next = n;
            lastNode = n;
            size++;
        }

    }

    public void removeFirst() {
        if (size != 0) {
            header.next = header.next.next;
            size--;
        }
    }

    public void removeLast() {
        if (size == 1) {
            header.next = null;
            lastNode = header;
            size--;
        } else if (size != 0) {
            Node00 n = header.next;
        }
    }

    public int getSize() {
        return size;
    }

    @Override
    public String toString() {
        Node00 n = header.next;
        String temp = "";
        while (n != null) {
            temp += n.data + " ";
            n = n.next;

        }

        return temp;
    }

}
